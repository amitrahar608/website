// import Link from 'next/link'
import Head from 'next/head'

function Navbar() {
  return <div>
    <Head>
      <title>UPSCALE</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"></link>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    </Head>
    <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
      <a className="navbar-brand" href="#">Upscale</a>

      <ul className="navbar-nav">
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
            Why Upscale
      </a>
          <div className="dropdown-menu">
            <a className="dropdown-item" href="#">Link 1</a>
            <a className="dropdown-item" href="#">Link 2</a>
            <a className="dropdown-item" href="#">Link 3</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
            Solutions
      </a>
          <div className="dropdown-menu">
            <a className="dropdown-item" href="#">Link 1</a>
            <a className="dropdown-item" href="#">Link 2</a>
            <a className="dropdown-item" href="#">Link 3</a>
          </div>
        </li>

        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
            Resources
      </a>
          <div className="dropdown-menu">
            <a className="dropdown-item" href="#">Link 1</a>
            <a className="dropdown-item" href="#">Link 2</a>
            <a className="dropdown-item" href="#">Link 35</a>
          </div>
        </li>
      </ul>
      <div className="set">
        <button type="button" class="btn btn-outline-secondary">SIGN IN</button>
        <button type="button" class="btn btn-primary">GET STARTED</button>
      </div>
    </nav>
  </div>
}

export default Navbar