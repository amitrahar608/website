import React from 'react';
import Link from 'next/link';
import first from '../../assets/images/first.jpg';
import up2 from '../../assets/images/up2.jpg';
import up3 from '../../assets/images/up3.jpg';
import up4 from '../../assets/images/up4.jpg';
import up5 from '../../assets/images/up5.jpg';
import salesforce from '../../assets/images/salesforce.svg';
import gmail from '../../assets/images/gmail.jpg';
import outlook from '../../assets/images/outlook.jpg';
import linkedin from '../../assets/images/linkedin.jpg';
import cal from '../../assets/images/cal.jpg';
import intercom from '../../assets/images/intercom.jpg';
import crunchbase from '../../assets/images/crunchbase.jpg';


function LandingPage() {

  return (
    <>
      <div className="col-12 col-md-12 landing-page top">
        <span className="set">SALES ENGAGEMENT PLATFORM</span>
        <h2 className="new">Upscale accelerates B2B sales
      <br></br> pipeline, for sales leaders</h2>
        <p className="para">Improve sales and marketing impact with personalised communication,
        actionable sales
      <br></br> activity insights that help you create a strong B2B
        sales pipeline and close deals faster.</p>
        <img className="img" src={first} alt={first} />
        <style jsx>
          {`
              .top{
                text-align: center;
                margin-top: 40px;
                color: #0f1e2d;
              }
              .set{
                  font-family: Roboto;
                  font-size: 10px;
                  line-height: 0.75;
              }
              .new{
                  font-size: 45px;
                  font-weight: bold;
                  line-height: 1.25;
                  text-align: center;
              }
              .para{
                  opacity: 0.6;
                  font-size: 17px;
                  font-family: Lato;
                  line-height: 1.45;
              }
              .img{
                width: 90%;
                height:90%;
              }
              `}
        </style>
      </div>
      {/*  */}

      <div className="row">
        <div className="col-12 col-md-12">
          <div class="card">
            <div className="bg">
              <div className="col-12 col-md-12 top-margin">
                <h2 className="new">Drive your sales team performance</h2>
                <p className="para">With Upscale, your sales team can spend more
        time building relationships and closing revenue</p>
              </div>
              <div className="row">
                <div className="col-12 col-md-4 set">

                  <img className="imgsecond" src={up2} alt={up2} />
                  <p className="para-top">Account-Based Sales, with Precision</p>
                  <span className="para-bootom">Upscale helps focused acquisition,
                retention and expansion of key accounts to scale, manage opportunities</span>

                </div>
                {/*  */}
                <div className="col-12 col-md-4 set">

                  <img className="imgsecond" src={up3} alt={up3} />
                  <p className="para-top">Personalised Multi-Channel Prospecting</p>
                  <span className="para-bootom">Increase sales pipeline with personalised sales outreach via
                 a multi-channel approach, be it email, phone, chat, etc.</span>

                </div>
                {/*  */}
                <div className="col-12 col-md-4 set">

                  <img className="imgsecond" src={up4} alt={up4} />
                  <p className="para-top">Inbound Sales Automation </p>
                  <span className="para-bootom">Upscale helps sales team engage with inbound prospects
                based on prioritization and buyer signals, real quick.</span>

                </div>
              </div>
              <style jsx>
                {`
                  .bg{
                    background-color: #f8f8f8;
                  }
                  .top-margin{
                    margin-top: 100px;
                    text-align: center;
                  }
                  .imgsecond{
                    width: 60%;
                    height:60%;
                    background-color: #f8f8f8;
                    box-shadow: 5px 10px 18px #888888 ;
                    border-radius: 10px;
                  }
                  .set{
                    text-align: center;
                    margin-top: 40px;
                    margin-bottom: 100px;
                    padding: 40px;
                  }
                  .para-top{
                      font-family: Roboto;
                      font-size: 17px;
                      font-weight: bold;
                      line-height: 3.2;
                      color: #0f1e2d;
                  }
                  .para-bootom{
                      opacity: 0.6;
                      font-family: Roboto;
                      font-size: 17px;
                      line-height: 1.45;
                      color: #0f1e2d;
                  }
                  `}
              </style>
            </div>
          </div>
        </div>
      </div>
      {/*  */}

      <div className="col-12 col-md-12">
        <img className="img11" src={up5} alt={up5} />
        <style jsx>
          {`
            .img11{
            width: 100%;
            height:100%;
            }
            `}
        </style>
      </div>
      {/*  */}

      <div className="row">
        <div className="col-12 col-md-12">
          <div class="card">
            <div className="bg">
              <div className="row">
                <div className="col-12 col-md-6 top-left">
                  <h2 className="new-first">Comprehensive <br></br> Sales Engagement</h2>
                  <p className="new-second">Integrated platform to radically improve your <br></br> sales reps efficiency</p>
                </div>
                <div className="col-12 col-md-6 top-right">
                  <div className="row">
                    <div className="col-12 col-md-6">
                      <p className="new-second">AI-Powered Sales Automation</p>
                    </div>
                    <div className="col-12 col-md-6">
                      <p className="new-second">Sales Performance Reporting</p>
                    </div>
                    <div className="col-12 col-md-6 ">
                      <p className="new-second">Multi-Channel Sales Outreach</p>
                    </div>
                    <div className="col-12 col-md-6 ">
                      <p className="new-second">A/B  Testing & Optimization</p>
                    </div>
                    <div className="col-12 col-md-6 ">
                      <p className="new-second">Intelligent Sales Workflows</p>
                    </div>
                    <div className="col-12 col-md-6 ">
                      <p className="new-second">Unified Sales Interface</p>
                    </div>
                  </div>
                </div>
              </div>
              <style jsx>
                {`
                    .bg{
                      background-color: #0f1e2d;
                    }
                    .top-left{
                      margin-top: 100px;
                    }
                    .top-right{
                      margin-top: 80px;
                    }
                    .new-first{
                        font-family: Lato;
                        font-size: 40px;
                        font-weight: bold;
                        line-height: 1.35;
                        color: white;
                        padding: 40px;
                    }
                    .new-second{
                        font-family: Roboto;
                        font-size: 18px;
                        line-height: 1.45;
                        padding: 40px;
                        color: white;
                    }
                    `}
              </style>
            </div>
          </div>
        </div>
      </div>
      {/*  */}

      <div className="row">
        <div className="col-12 col-md-12">
          <div class="card">
            <div className="bg">
              <div className="col-12 col-md-12 roww">
                <h2 className="set-top">Integrate like a boss</h2>
                <p className="set-bootom">Personalize, optimize and act on your data with the products you already use</p>
              </div>
              <p className="link"><a href="#">See All Integrations</a></p>
              <div className="row">
                <div className="col-12 col-md-3 cet">
                  <img className="img-third" src={salesforce} alt={salesforce} />
                </div>
                <div className="col-12 col-md-3 cet">
                  <img className="img-third" src={outlook} alt={outlook} />
                </div>
                <div className="col-12 col-md-3 cet">
                  <img className="img-third" src={outlook} alt={outlook} />
                </div>
                <div className="col-12 col-md-3 cet">
                  <img className="img-third" src={salesforce} alt={salesforce} />
                </div>
                <div className="col-12 col-md-3 cet">
                  <img className="img-third" src={outlook} alt={outlook} />
                </div>
                <div className="col-12 col-md-3 cet">
                  <img className="img-third" src={salesforce} alt={salesforce} />
                </div>
                <div className="col-12 col-md-3 cet">
                  <img className="img-third" src={outlook} alt={outlook} />
                </div>
                <div className="col-12 col-md-3 cet">
                  <img className="img-third" src={salesforce} alt={salesforce} />
                </div>
              </div>
              <style jsx>
                {`
                      .bg{
                      }
                      .roww{
                        margin-top: 100px;
                      }
                      .cet{
                        text-align: center;
                        margin-top: 70px;
                      }
                      .set-top{
                          font-family: Lato;
                          font-size: 40px;
                          font-weight: bold;
                          line-height: 1.35;
                          text-align: center;
                          color: #0f1e2d;
                      }
                      .set-bootom{
                          opacity: 0.6;
                          font-family: Roboto;
                          font-size: 21px;
                          line-height: 1.45;
                          text-align: center;
                          color: #0f1e2d;
                        }
                        .link{
                            font-family: Lato;
                            font-size: 24px;
                            font-weight: 800;
                            line-height: 0.6;
                            color: #2190ff;
                            text-align: center;
                        }
                        .img-third{
                          border-radius: 10px;
                          box-shadow: 5px 10px 18px #888888 ;
                          margin: 20px 0;
                          padding: 20px;
                          padding: 15px!important;
                          cursor: pointer;
                        }
                      }
                      `}
              </style>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default LandingPage;
