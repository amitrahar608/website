import Link from 'next/link'
import Head from 'next/head'

import HeaderNavbar from '../components/HeaderNavbar'
import LandingPage from '../components/LandingPage'

const index = () => {
  return <div>
    <HeaderNavbar></HeaderNavbar>
    <LandingPage></LandingPage>
  </div>
}

export default index